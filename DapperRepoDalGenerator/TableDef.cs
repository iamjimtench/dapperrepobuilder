﻿using System.Collections.Generic;

namespace DapperRepoDalGenerator
{
    public class TableDef
    {
        public List<DataField> fields;
        public string tableName;
        public List<DataField> keyFields;
        public List<DataField> nonKeyFields;
        public int precedenceLevel;
        public bool IsIdentityTable;
        public bool IsMultiKeyTable;
        public List<Reference> referencingTables;
        public bool IsReferenced;
        public bool IsLinkTable;
    }
}