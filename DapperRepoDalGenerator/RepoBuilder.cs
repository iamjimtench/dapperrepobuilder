﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperRepoDalGenerator
{
    public class RepoBuilder
    {
		TableDef tableDef;
		string pocoNamespace;
        string repoNamespace;
		string indent1 = "    ";
		string indent2 = "        ";
		string indent3 = "            ";

		public RepoBuilder(TableDef tableDef, string pocoNamespace, string repoNamespace)
        {
            this.tableDef = tableDef;
            this.pocoNamespace = pocoNamespace;
            this.repoNamespace = repoNamespace;
        }

        public List<string> BuildRepoDef(TableDef tableDef)
        {
			Tuple<string, string> retTuple;
			List<string> lines = GetRepoHeader(tableDef.tableName);

            #region Constructor
            lines.Add(indent2 + "private string connectionString;");
			lines.Add(indent2 + "IDbConnection con;");
			lines.Add("");
			lines.Add(indent2 + "public " + "Crud" + tableDef.tableName + "Repo(string connectionString)");
			lines.Add(indent2 + "{");
			lines.Add(indent3 + "this.connectionString = connectionString;");
			lines.Add(indent3 + "con = new SqlConnection(connectionString);");
			lines.Add(indent2 + "}");
			lines.Add("");
			#endregion

			#region GetAll
			lines.Add(indent2 + "public IEnumerable<" + tableDef.tableName + "> GetAll()");
			lines.Add(indent2 + "{");
			lines.Add(indent3 + "string SQL = \"" + GetSelectAllSql(tableDef.tableName, tableDef.nonKeyFields, tableDef.keyFields) + "\";");
			lines.Add(indent3 + "return con.Query<" + tableDef.tableName + ">(SQL);");
			lines.Add(indent2 + "}");
			lines.Add("");
			#endregion

			#region Get by ID
			lines.Add(indent2 + "public " + tableDef.tableName + " GetById(int Id)");
			lines.Add(indent2 + "{");
			retTuple = GetSelectSqlByID(tableDef.tableName, tableDef.nonKeyFields, tableDef.keyFields);
			lines.Add(indent3 + "string SQL = \"" + retTuple.Item1 + ";");
			lines.Add(indent3 + "var ret = con.Query<" + tableDef.tableName + ">(SQL, " + retTuple.Item2 + "}).AsList();");
			lines.Add(indent3 + "return ret[0];");
			lines.Add(indent2 + "}");
			lines.Add("");
			#endregion

			#region Get Field By Name
			lines.Add(indent2 + "public " + tableDef.tableName + " GetByFieldName(string FieldName, string FieldValue)");
			lines.Add(indent2 + "{");
			lines.Add(indent3 + "var ret = con.Query<" + tableDef.tableName + ">(\"" + GetSelectByNameSql(tableDef.tableName, tableDef.nonKeyFields, tableDef.keyFields) + ").AsList();");
			lines.Add(indent3 + "return ret[0];");
			lines.Add(indent2 + "}");
			lines.Add("");
			#endregion

			#region Insert 
			lines.Add(indent2 + "public int Add(" + tableDef.tableName + " Entity)");
			lines.Add(indent2 + "{");
			if (TestIfIdentity(tableDef))
			{
				lines.Add(indent3 + "var ret = con.Query<int>(\"" + GetInsertIdentitySql(tableDef.tableName, tableDef.nonKeyFields, tableDef.keyFields) + ").AsList();");
				lines.Add(indent3 + "return ret[0];");
			}
			else 
			{
				lines.Add(indent3 + "return con.Execute(\"" + getInsertNonIdentitySql(tableDef.tableName, tableDef.nonKeyFields, tableDef.keyFields) + "); ");
			}
			lines.Add(indent2 + "}");
			lines.Add("");
			#endregion

			#region Update
			lines.Add(indent2 + "public void Update(" + tableDef.tableName + " Entity)");
			lines.Add(indent2 + "{");
			lines.Add(indent3 + "con.Execute(\"" + getUpdateSql(tableDef.tableName, tableDef.nonKeyFields, tableDef.keyFields) + ");");
			lines.Add(indent2 + "}");
			#endregion

			#region Delete
			lines.Add(indent2 + "public void Delete(" + tableDef.tableName + " Entity)");
			lines.Add(indent2 + "{");
			lines.Add(indent3 + "con.Execute(\"" + GetDeleteFromSql(tableDef.tableName, tableDef.nonKeyFields, tableDef.keyFields) +");");
			lines.Add(indent2 + "}");
            #endregion
            
			lines.Add(indent1 + "}");
			lines.Add("}");
		
			return lines;
		}

		public string GetSelectAllSql(string TableName, List<DataField> NonKeyFields, List<DataField> KeyFields)
		{
		string selectAllSql = "SELECT ";

		foreach (DataField df in NonKeyFields)
		{
			selectAllSql += " " + df.value + ",";
		}

		foreach (DataField df in KeyFields)
		{
			selectAllSql += " " + df.value + ",";
		}
		selectAllSql = " " + selectAllSql.Substring(0, selectAllSql.Length - 1);
		selectAllSql += " FROM " + TableName;

		return selectAllSql;
		}

		public string getUpdateSql(string TableName, List<DataField> NonKeyFields, List<DataField> KeyFields)
		{
		string updateSql = "UPDATE " + TableName + " SET ";
		string whereSql = " WHERE ";
		string newObjectSql = @""", new {";

		foreach (DataField df in NonKeyFields)
		{
			updateSql += " " + df.value + " = @" + df.value + ",";
			newObjectSql += df.value + "= Entity." + df.value + ",";
		}
		foreach (DataField df in KeyFields)
		{
			newObjectSql += df.value + "= Entity." + df.value + ",";
			whereSql += df.value + " = @" + df.value + ",";
		}

		updateSql = updateSql.Substring(0, updateSql.Length - 1);
		whereSql = whereSql.Substring(0, whereSql.Length - 1);
		newObjectSql = newObjectSql.Substring(0, newObjectSql.Length - 1) + " }";

		return updateSql + whereSql + newObjectSql;
		}

		public string getInsertNonIdentitySql(string TableName, List<DataField> NonKeyFields, List<DataField> KeyFields)
		{
		string insertSql = "INSERT INTO " + TableName + "(";
			string valuesSql = " Values(";
		string newObjectSql = @""", new {";

		foreach (DataField df in KeyFields)
		{
			insertSql += df.value + ",";
			valuesSql += @"@" + df.value + ",";
			newObjectSql += df.value + " = @Entity." + df.value + ",";
		}
		foreach (DataField df in NonKeyFields)
		{
			insertSql += df.value + ",";
			valuesSql += @"@" + df.value + ",";
			newObjectSql += df.value + " = Entity." + df.value + ",";
		}
		insertSql = insertSql.Substring(0, insertSql.Length - 1) + ") ";
		newObjectSql = newObjectSql.Substring(0, newObjectSql.Length - 1) + " }";
		valuesSql = valuesSql.Substring(0, valuesSql.Length - 1);

		return insertSql + valuesSql + newObjectSql;

		}

		public string GetInsertIdentitySql(string TableName, List<DataField> NonKeyFields, List<DataField> KeyFields)
		{
		string insertSql = "INSERT INTO " + TableName + "(";
		string valuesSql = " Values(";
		string newObjectSql = @""", new {";

		foreach (DataField df in KeyFields)
		{
			if (!df.isIdentity)
			{
				insertSql += df.value + ",";
				valuesSql += @"@" + df.value + ",";
				newObjectSql += df.value + " = Entity." + df.value + ",";
			}
		}
		foreach (DataField df in NonKeyFields)
		{
			insertSql += df.value + ",";
			valuesSql += @"@" + df.value + ",";
			newObjectSql += df.value + " = Entity." + df.value + ",";
		}
		insertSql = insertSql.Substring(0, insertSql.Length - 1) + ") ";
		newObjectSql = newObjectSql.Substring(0, newObjectSql.Length - 1) + " }";
		valuesSql = valuesSql.Substring(0, valuesSql.Length - 1) + @"); SELECT CAST(SCOPE_IDENTITY() AS INT) ";

		return insertSql + valuesSql + newObjectSql;
		}

		public string GetDeleteFromSql(string TableName, List<DataField> NonKeyFields, List<DataField> KeyFields)
		{
		string selectSql = "DELETE ";
		string whereSql = "";
		string newObectSql = @""", new {";

		for (int i = 0; i < KeyFields.Count; i++)
		{
			if (i > 0)
			{
				whereSql += " AND ";
			}
			else
			{
				whereSql = " WHERE ";
			}
			whereSql += " " + KeyFields[i].value + " = @" + KeyFields[i].value;
			newObectSql += " Entity." + KeyFields[i].value + ",";
		}
		selectSql = selectSql.Substring(0, selectSql.Length - 1);
		newObectSql = newObectSql.Substring(0, newObectSql.Length - 1);

		return selectSql + " FROM " + TableName + whereSql + newObectSql + " }";
		}

		public string GetSelectByNameSql(string TableName, List<DataField> NonKeyFields, List<DataField> KeyFields)
		{
		string selectSql = "SELECT ";
		string whereSql = @" WHERE "" + FieldName + "" = @fieldValue"", new { fieldValue = FieldValue }";

		for (int i = 0; i < KeyFields.Count; i++)
		{
			selectSql += " " + KeyFields[i].value + ",";
		}

		foreach (DataField df in NonKeyFields)
		{
			selectSql += " " + df.value + ",";
		}

		return selectSql.Substring(0, selectSql.Length - 1) + " FROM " + TableName + whereSql;
		}

		public Tuple<string, string> GetSelectSqlByID(string TableName, List<DataField> NonKeyFields, List<DataField> KeyFields)
		{
		string selectSql = "SELECT ";
		string whereSql = " WHERE ";
		string newObectSql = @"new {";

		for (int i = 0; i < KeyFields.Count; i++)
		{
			if (i > 0)
			{
				whereSql += " AND";
			}
			selectSql += " " + KeyFields[i].value + ",";
			whereSql += " " + KeyFields[i].value + " = @" + KeyFields[i].value;
			newObectSql += " " + KeyFields[i].value + " = Id ,";
		}

		foreach (DataField df in NonKeyFields)
		{
			selectSql += " " + df.value + ",";
		}
		selectSql = selectSql.Substring(0, selectSql.Length - 1);
		newObectSql = newObectSql.Substring(0, newObectSql.Length - 1);

		return new Tuple<string, string>(selectSql + " FROM " + TableName + whereSql + "\"", newObectSql);
		}

		private List<string> GetRepoHeader(string tableName)
		{
		List<string> lines = new List<string>();

		lines.Add("using System;");
		lines.Add("using Dapper;");
		lines.Add("using " + pocoNamespace + ";");
		lines.Add("using System.Data;");
		lines.Add("using System.Collections.Generic;");
		lines.Add("using System.Data.SqlClient;");
		lines.Add("");
		lines.Add("namespace Crud" + repoNamespace);
		lines.Add("{");
		lines.Add(indent1 + "public class " + "Crud" + tableName + "Repo");
		lines.Add(indent1 + "{");

		return lines;
		}

		public bool TestIfIdentity(TableDef tableDetails)
		{
			bool isIdentity = false;
			foreach (DataField df in tableDetails.fields)
			{
				if (df.isIdentity == true)
				{
					isIdentity = true;
					break;
				}
			}
			return isIdentity;
		}
	}
}
