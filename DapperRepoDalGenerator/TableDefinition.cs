﻿using System;
using System.Collections.Generic;

namespace DapperRepoDalGenerator
{
    public class TableDefinitionBuilder
    {
        private IDbAccess db;
        Dictionary<string, int> precedenceLevels;
        Dictionary<string, List<Reference>> references; 

        public TableDefinitionBuilder(IDbAccess db)
        {
            this.db = db;
            precedenceLevels = db.GetPrecedenceLevels();
            references = db.BuildReferencesDictionary();
        }

        public TableDef BuildTableDef(string tableName)
        { 
            TableDef td = new TableDef();
            td.tableName = tableName;
            td.fields = db.GetTableDetails(tableName);
            td.nonKeyFields = GetNonKeyFields(td.fields);
            td.precedenceLevel = precedenceLevels[tableName];
            td.keyFields = GetKeyFields(td.fields);
            td.IsIdentityTable = TestIfIdentity(td);
            td.IsMultiKeyTable = TestIfHasMultipleKeys(td);

            if (references.ContainsKey(tableName))
            {
                td.IsReferenced = true;
                td.referencingTables = references[tableName];
            }

            if (td.keyFields.Count == 2 && td.nonKeyFields.Count == 0)
            {
                td.IsLinkTable = true;
            }
            else
            {
                td.IsLinkTable = false;
            }

            return td;
        }

        public bool? TestIfLinkTable(TableDef tableDef)
        {
            int numKeyFields = GetKeyFields(tableDef.fields).Count;
            int numNonKeyFields = GetNonKeyFields(tableDef.fields).Count;

            if (numKeyFields == 2 && numNonKeyFields == 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool TestIfHasMultipleKeys(TableDef tableDef)
        {
            if (tableDef.keyFields.Count > 1)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool TestIfIdentity(TableDef tableDetails)
        {
            bool isIdentity = false;
            foreach (DataField df in tableDetails.fields)
            {
                if (df.isIdentity == true)
                {
                    isIdentity = true;
                    break;
                }
            }
            return isIdentity;
        }

        public List<string> GatListAssociatedTables(TableDef tableDef)
        {
            List<Reference> tableRefs = references[tableDef.tableName];

            if (tableRefs == null)
            {
                return null;
            }
            else
            {
                List<string> tableNames = new List<string>();
                foreach (Reference rf in tableRefs)
                {
                    tableNames.Add(rf.foreignTable);
                }
                return tableNames;
            }
        }

        public List<DataField> GetNonKeyFields(List<DataField> list)
        {
            List<DataField> fields = new List<DataField>();

            foreach (DataField df in list)
            {
                if (!df.isPrimaryKey)
                { fields.Add(df); }
            }

            return fields;
        }

        public List<DataField> GetKeyFields(List<DataField> list)
        {
            List<DataField> fields = new List<DataField>();

            foreach (DataField df in list)
            {
                if (df.isPrimaryKey)
                { fields.Add(df); }
            }

            return fields;
        }

    }
}
