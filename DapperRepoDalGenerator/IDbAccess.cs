﻿using System.Collections.Generic;

namespace DapperRepoDalGenerator
{
    public interface IDbAccess
    {
        List<string> GetListTables();
        Dictionary<string, int> GetPrecedenceLevels();
        List<DataField> GetTableDetails(string tableName);
        Dictionary<string, List<Reference>> BuildReferencesDictionary();
    }
}