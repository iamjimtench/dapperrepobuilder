﻿namespace DapperRepoDalGenerator
{
    public class Reference
    {
        public string foreignKeyName;
        public string foreignTable;
        public string foreignColumn;
        public string parentTable;
        public string parentColumn;
    }
}