﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperRepoDalGenerator
{
    public class PocoBuilder
    {
        private string pocoNamespace;

        public PocoBuilder(string pocoNamespace)
        {
            this.pocoNamespace = pocoNamespace;
        }

        public List<string> BuildPocoDef(TableDef tableDef)
        {
            List<string> lines = GetPocoHeader(tableDef.tableName);

            foreach (DataField df in tableDef.keyFields)
            {
                lines.Add("        public " + GetClrType(df.dataType) + " " + df.value + " { get; set; } // Key");
            }

            foreach (DataField df in tableDef.nonKeyFields)
            {
                lines.Add("        public " + GetClrType(df.dataType) + " " + df.value + " { get; set; }");
            }

            lines.Add("    }");
            lines.Add("}");

            return lines;
        }

        private List<string> GetPocoHeader(string tableName)
        {
            List<string> lines = new List<string>();

            lines.Add("using System;");
            lines.Add("");
            lines.Add("namespace " + pocoNamespace);
            lines.Add("{");
            lines.Add("    " + "public class " + tableName);
            lines.Add("    {");

            return lines;
        }

        public string GetClrType(string sqlType)
        {
            switch (sqlType)
            {
                case "bigint":
                    return "long";

                case "binary":
                case "image":
                case "timestamp":
                case "varbinary":
                    return "byte[]";

                case "bit":
                    return "bool";

                case "char":
                case "nchar":
                case "ntext":
                case "nvarchar":
                case "text":
                case "varchar":
                case "xml":
                    return "string";

                case "datetime":
                case "smalldatetime":
                case "date":
                case "time":
                case "datetime2":
                    return "DateTime";

                case "decimal":
                case "money":
                case "smallmoney":
                    return "decimal";

                case "float":
                    return "double";

                case "int":
                    return "int";

                case "real":
                    return "float";

                case "uniqueidentifier":
                    return "guid";

                case "smallint":
                    return "short";

                case "tinyint":
                    return "byte";

                case "variant":
                case "udt":
                    return "object";

                case "structured":
                    return "DataTable";

                case "datetimeoffset":
                    return "DateTimeOffset";
                default:
                    throw new ArgumentOutOfRangeException("sqlType");
            }
        }

    }
}
