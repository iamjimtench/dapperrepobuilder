﻿using System.Collections.Generic;

namespace DapperRepoDalGenerator
{
    public class DataField
    {
        public string dataType;
        public string value;
        public bool isIdentity;
        public bool isNullable;
        public bool isPrimaryKey;
        public bool isForeighKey;
    }
}