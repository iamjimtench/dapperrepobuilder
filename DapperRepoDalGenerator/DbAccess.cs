﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DapperRepoDalGenerator
{
    public class DbAccess : IDbAccess
    {
        private SqlConnection con;

        public DbAccess(string conString)
        {
            con = new SqlConnection(conString);
        }


        public List<string> GetListTables()
        {
            DataTable dtTables = new DataTable();
            using (SqlDataAdapter da = new SqlDataAdapter(tableSQL, con))
            {
                da.Fill(dtTables);
            }

            List<string> ret = new List<string>();
            foreach (DataRow row in dtTables.Rows)
            {
                ret.Add(row["name"].ToString());
            }
            return ret;
        }

        public Dictionary<string, int> GetPrecedenceLevels()
        {
            Dictionary<string, int> ret = new Dictionary<string, int>();

            DataTable dt = new DataTable();

            using (SqlDataAdapter da = new SqlDataAdapter(TablesByDepenencySql, con))
            {
                da.Fill(dt);
            }

            foreach (DataRow row in dt.Rows)
            {
                ret.Add(row["tableName"].ToString(), int.Parse(row["level"].ToString()));
            }

            return ret;
        }

        public Dictionary<string, List<Reference>> BuildReferencesDictionary()
        {
            Dictionary<string, List<Reference>> references = new Dictionary<string, List<Reference>>();
            DataTable dt = new DataTable();
            using (SqlDataAdapter da = new SqlDataAdapter(referencesSql, con))
            {
                da.Fill(dt);
            }

            foreach(DataRow row in dt.Rows)
            {
                Reference reference = new Reference();
                reference.foreignColumn = row["foreign_column"].ToString();
                reference.foreignKeyName = row["foreign_key_name"].ToString();
                reference.foreignTable = row["foreign_table"].ToString();
                reference.parentTable = row["parent_table"].ToString();
                reference.parentColumn = row["parent_column"].ToString();
                if (references.ContainsKey(reference.parentTable))
                {
                    references[reference.parentTable].Add(reference);
                }
                else
                {
                    List<Reference> newRef = new List<Reference>();
                    newRef.Add(reference);
                    references.Add(reference.parentTable, newRef);
                }
            }

            return references;
        }

        public List<DataField> GetTableDetails(string tableName)
        {
            DataTable dt = new DataTable();
            List<DataField> dataFields = new List<DataField>();

            using (SqlDataAdapter da = new SqlDataAdapter(detailsSQL + tableName + "')", con))
            {
                da.Fill(dt);
                foreach (DataRow dr in dt.Rows)
                {
                    DataField df = new DataField();
                    df.dataType = dr["Datatype"].ToString();
                    df.value = dr["ColumnName"].ToString();
                    df.isNullable = (bool)dr["Nullable"];
                    df.isIdentity = (bool)dr["Identity"];
                    df.isPrimaryKey = (bool)dr["PrimaryKey"];
                    dataFields.Add(df);
                }
            }
            return dataFields;
        }


        public string GetDbConfString(string conStringName)
        {
            return ConfigurationManager.ConnectionStrings[conStringName].ToString();
        }





        #region SqlStrings
 
        string tableSQL = @"
SELECT name FROM sys.tables
WHERE name <> 'sysdiagrams'
AND name <> '__RefactorLog'";

        string detailsSQL = @"
SELECT 
    c.name 'ColumnName',
    t.Name 'Datatype',
    c.is_nullable 'Nullable',
    c.is_identity 'Identity',
    ISNULL(i.is_primary_key, 0) 'PrimaryKey'
FROM    
    sys.columns c
INNER JOIN 
    sys.types t ON c.user_type_id = t.user_type_id
LEFT OUTER JOIN 
    sys.index_columns ic ON ic.object_id = c.object_id AND ic.column_id = c.column_id
LEFT OUTER JOIN 
    sys.indexes i ON ic.object_id = i.object_id AND ic.index_id = i.index_id
WHERE
    c.object_id = OBJECT_ID('";

        //Borrowed from http://sqlblog.com/blogs/jamie_thomson/archive/2009/09/08/deriving-a-list-of-tables-in-dependency-order.aspx
        private string TablesByDepenencySql = @"
with fk_tables as (
    select    s1.name as from_schema    
    ,        o1.Name as from_table    
    ,        s2.name as to_schema    
    ,        o2.Name as to_table    
    from    sys.foreign_keys fk    
    inner    join sys.objects o1    
    on        fk.parent_object_id = o1.object_id    
    inner    join sys.schemas s1    
    on        o1.schema_id = s1.schema_id    
    inner    join sys.objects o2    
    on        fk.referenced_object_id = o2.object_id    
    inner    join sys.schemas s2    
    on        o2.schema_id = s2.schema_id    
    /*For the purposes of finding dependency hierarchy       
        we're not worried about self-referencing tables*/
    where    not    (    s1.name = s2.name                 
            and        o1.name = o2.name)
)
,ordered_tables AS 
(        SELECT    s.name as schemaName
        ,        t.name as tableName
        ,        0 AS Level    
        FROM    (    select    *                
                    from    sys.tables                 
                    where    name <> 'sysdiagrams') t    
        INNER    JOIN sys.schemas s    
        on        t.schema_id = s.schema_id    
        LEFT    OUTER JOIN fk_tables fk    
        ON        s.name = fk.from_schema    
        AND        t.name = fk.from_table    
        WHERE    fk.from_schema IS NULL
        UNION    ALL
        SELECT    fk.from_schema
        ,        fk.from_table
        ,        ot.Level + 1    
        FROM    fk_tables fk    
        INNER    JOIN ordered_tables ot    
        ON        fk.to_schema = ot.schemaName    
        AND        fk.to_table = ot.tableName
)select    distinct    ot.schemaName
,        ot.tableName
,        ot.Level
from    ordered_tables ot
inner    join (
        select    schemaName
        ,        tableName
        ,        MAX(Level) maxLevel        
        from    ordered_tables        
        group    by schemaName,tableName
        ) mx
on        ot.schemaName = mx.schemaName
and        ot.tableName = mx.tableName
and        mx.maxLevel = ot.Level
";

        private string referencesSql = @"
select cast(f.name as varchar(255)) as foreign_key_name
, cast(c.name as varchar(255)) as foreign_table
, cast(fc.name as varchar(255)) as foreign_column
, cast(p.name as varchar(255)) as parent_table
, cast(rc.name as varchar(255)) as parent_column
from  sysobjects f
inner join sysobjects c on f.parent_obj = c.id
inner join sysreferences r on f.id = r.constid
inner join sysobjects p on r.rkeyid = p.id
inner join syscolumns rc on r.rkeyid = rc.id and r.rkey1 = rc.colid
inner join syscolumns fc on r.fkeyid = fc.id and r.fkey1 = fc.colid
where f.type = 'F'";
        #endregion

    }
}
