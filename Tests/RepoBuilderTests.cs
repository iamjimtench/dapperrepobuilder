﻿using DapperRepoDalGenerator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    class RepoBuilderTests
    {
        private RepoBuilder rb;

        [OneTimeSetUp]
        public void RepoBuilderTestInit()
        {
            TableDef td = new TableDef();
            rb = new RepoBuilder(td, "Pocos", "Repos");
            //rb.BuildTableDefDictionary();
        }

        [Test]
        public void ShouldBuildDictionaryOfTableDefs()
        {
            //Assert.IsTrue(rb.BuildTableDefDictionary());
        }

        [Test]
        public void ShouldReturnListOfRepoMethods()
        {
            //Assert.IsTrue(rb.ReturnAllMethodStringLists().Count > 10);
        }

        [Test]
        public void GivenTableName_ShouldConfirmTableType()
        {
            /*Assert.Multiple(() =>
            {
                Assert.IsFalse(rb.TestIsLinkTable("Program"));
                Assert.IsTrue(rb.TestIsLinkTable("CastInProgram"));
                Assert.IsTrue(rb.TestHasRelatedTables("Program"));
            });*/
        }

        [Test]
        public void GivenStandardTableName_ShouldReturnAddMthodStrings()
        {
            List<string> test = GetTestStandardAddMethodStrings();
            /*List<List<string>> ret = rb.BuildAddMethod("CastType");
            Assert.Multiple(() =>
            {
                Assert.AreEqual(ret.Count, 6);
                Assert.AreEqual(ret[0][3], "");
                Assert.IsTrue(ret[1].Count == 0);
            });*/
        }

        [Test]
        public void GivenTableWithChildren_ShouldReturnAddMethodStrings()
        {
            List<List<string>> test = GetTestTableWithChildrenStrings();
            /*List<List<string>> ret = rb.BuildAddMethod("Cast");

            Assert.Multiple(() =>
            {
                Assert.AreEqual(ret.Count, 2);
                Assert.IsTrue(ret[1][5] == "");
                Assert.IsTrue(ret[0][5] == "");
            });*/
        }

        #region Test Data

        private List<string> GetTestStandardAddMethodStrings()
        {
            List<string> test = new List<string>();

            return test;
        }

        private List<List<string>> GetTestTableWithChildrenStrings()
        {
            List<List<string>> ret = new List<List<string>>();

            return ret;
        }

        #endregion
    }
}
