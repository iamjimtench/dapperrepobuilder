﻿using DapperRepoDalGenerator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    class DbAccessTests
    {
        private DbAccess db;

        [OneTimeSetUp]
        public void initDbAccess()
        {
            string conName = "TvDbConString";
            db = new DbAccess(conName);
        }

        [Test]
        public void ShouldReturnListOftables()
        {
            List<string> tables = db.GetListTables();
            Assert.IsTrue(tables.Count > 5);
        }

        [Test]
        public void ShouldReturnPrecedenceLevels()
        {
            Dictionary<string, int> levels = db.GetPrecedenceLevels();

            Assert.IsTrue(levels.Count > 0);
        }

        [Test]
        public void GivenTableName_ShouldReturnTableDetails()
        {
            string tableName = "Customer";
            List<DataField> details = db.GetTableDetails(tableName);
            Assert.IsTrue(details.Count > 2);
        }

        [Test]
        public void SHouldReturnAllReferences()
        {
            var references = db.BuildReferencesDictionary();

            Assert.IsTrue(references.Count > 0);
        }
    }
}
