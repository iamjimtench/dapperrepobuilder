﻿using DapperRepoDalGenerator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    class TableDefinitionBuilderTests
    {
        TableDefinitionBuilder td;
        TableDef singleKeyTable;
        TableDef multiKeyTable;

        [OneTimeSetUp]
        public void InitTableDefTests()
        {
            DbAccess db = new DbAccess("TvDbConString");
            td = new TableDefinitionBuilder(db);
            singleKeyTable = td.BuildTableDef("Program");
            multiKeyTable = td.BuildTableDef("CastInProgram");
        }

        [Test]
        public void GivenTableDetails_ShouldReturnNonKeyFields()
        {
            List<DataField> fields = td.GetNonKeyFields(singleKeyTable.fields);

            Assert.IsTrue(fields.Count > 1);
        }

        [Test]
        public void GivenTableDetails_ShouldReturnKeyFields()
        {
            List<DataField> fields = td.GetKeyFields(singleKeyTable.fields);

            Assert.IsTrue(fields.Count == 1);
        }

        [Test]
        public void GivenTableDetails_ShouldConfirmMultiKeyTable()
        {
            bool test = td.TestIfHasMultipleKeys(multiKeyTable);
            Assert.IsTrue(test);
        }

        [Test]
        public void GivenTableDetails_ShoudConfirmSingleKeyTable()
        {
            bool test = td.TestIfHasMultipleKeys(singleKeyTable);
            Assert.IsFalse(test);
        }

        [Test]
        public void GivenTableDef_ShouldConfirmIsLinkTable()
        {
            Assert.IsTrue(td.TestIfLinkTable(multiKeyTable));
        }

        [Test]
        public void GivenTableDef_ShouldConfirmIsNotLinkTable()
        {
            Assert.IsFalse(td.TestIfLinkTable(singleKeyTable));
        }

        [Test]
        public void GivenTableDef_ShouldReturnListOfLinkAscoiatedLinkTables()
        {
            Assert.IsTrue(td.GatListAssociatedTables(multiKeyTable).Count > 0);
        }

        [Test]
        public void GivenTableDef_ShouldReturnNoLitsAssociatedTable()
        {
            Assert.IsNull(td.GatListAssociatedTables(singleKeyTable));
        }
    }
}
