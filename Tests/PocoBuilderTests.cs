﻿using DapperRepoDalGenerator;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    [TestFixture]
    public class PocoBuilderTests
    {
        TableDef singleKeyTableDef;
        TableDef multiKeyTableDef;
        TableDefinitionBuilder td;
        PocoBuilder pb;

        [OneTimeSetUp]
        public void PocoBuilderTestsInit()
        {
            td = new TableDefinitionBuilder(new DbAccess("TvDbConString"));
            singleKeyTableDef = td.BuildTableDef("Customer");
            multiKeyTableDef = td.BuildTableDef("StockItemCategory");
            pb = new PocoBuilder(td, "Pocos");
        }

        [Test]
        public void GivenTableDetails_ShouldReturnSinglrKeyStringPocoDef()
        {
            List<string> testPocoDef = GetTestProgramPocoDef();

            List<string> pocoDef = pb.BuildPocoDef(singleKeyTableDef);

            Assert.AreEqual(testPocoDef.Count, pocoDef.Count);

        }

        [Test]
        public void GivenTableDetails_ShouldReturnStringMultiKeyPocoDef()
        {
            List<string> testPocoDef = GetTestCatInProgramDef();

            List<string> pocoDef = pb.BuildPocoDef(multiKeyTableDef);

            Assert.AreEqual(testPocoDef.Count, pocoDef.Count);

        }

        #region support methods
        private List<string> GetTestProgramPocoDef()
        {
            List<string> lines = new List<string>();

            lines.Add("using System;");
            lines.Add("");
            lines.Add("namespace Pocos");
            lines.Add("{");
            lines.Add("    public partial class Program");
            lines.Add("    {");
            lines.Add("        public int ID { get; set; } // Key");
            lines.Add("        public string ProgramName { get; set; }");
            lines.Add("        public string ShortDescription { get; set; }");
            lines.Add("        public string EpisodeTitle { get; set; }");
            lines.Add("        public string OriginalDate { get; set; }");
            lines.Add("        public DateTime PreviouslyShown { get; set; }");
            lines.Add("    }");
            lines.Add("}");

            return lines;
        }

        private List<string> GetTestCatInProgramDef()
        {
            List<string> lines = new List<string>();

            lines.Add("using System;");
            lines.Add("");
            lines.Add("namespace Pocos");
            lines.Add("{");
            lines.Add("    public partial class CastInProgram");
            lines.Add("    {");
            lines.Add("        public int CastID { get; set; } // Key");
            lines.Add("        public int ProgramID { get; set; } // Key");
            lines.Add("    }");
            lines.Add("}");

            return lines;
        }

        #endregion

    }
}
