﻿
namespace Gui
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtConString = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.lstTables = new System.Windows.Forms.ListBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtPocos = new System.Windows.Forms.RichTextBox();
            this.txtRepos = new System.Windows.Forms.RichTextBox();
            this.cmdLoadTables = new System.Windows.Forms.Button();
            this.cmdBuildPocos = new System.Windows.Forms.Button();
            this.cmdBuildRepos = new System.Windows.Forms.Button();
            this.cmdWriteRepos = new System.Windows.Forms.Button();
            this.cmdWritePocos = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtPathToRepos = new System.Windows.Forms.TextBox();
            this.txtPathToPocos = new System.Windows.Forms.TextBox();
            this.txtPocoNamespace = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.txtRepoNamespace = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // txtConString
            // 
            this.txtConString.Location = new System.Drawing.Point(12, 35);
            this.txtConString.Name = "txtConString";
            this.txtConString.Size = new System.Drawing.Size(242, 20);
            this.txtConString.TabIndex = 0;
            this.txtConString.Text = "Server=DESKTOP-HIQ1659;Database=StockDB;Trusted_Connection=True;";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(12, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(91, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Connection String";
            // 
            // lstTables
            // 
            this.lstTables.FormattingEnabled = true;
            this.lstTables.Location = new System.Drawing.Point(12, 94);
            this.lstTables.Name = "lstTables";
            this.lstTables.Size = new System.Drawing.Size(120, 472);
            this.lstTables.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(12, 67);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(53, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "Table List";
            // 
            // txtPocos
            // 
            this.txtPocos.Location = new System.Drawing.Point(161, 94);
            this.txtPocos.Name = "txtPocos";
            this.txtPocos.Size = new System.Drawing.Size(537, 472);
            this.txtPocos.TabIndex = 4;
            this.txtPocos.Text = "";
            // 
            // txtRepos
            // 
            this.txtRepos.Location = new System.Drawing.Point(730, 94);
            this.txtRepos.Name = "txtRepos";
            this.txtRepos.Size = new System.Drawing.Size(519, 472);
            this.txtRepos.TabIndex = 5;
            this.txtRepos.Text = "";
            // 
            // cmdLoadTables
            // 
            this.cmdLoadTables.Location = new System.Drawing.Point(12, 599);
            this.cmdLoadTables.Name = "cmdLoadTables";
            this.cmdLoadTables.Size = new System.Drawing.Size(75, 23);
            this.cmdLoadTables.TabIndex = 6;
            this.cmdLoadTables.Text = "Load Tables";
            this.cmdLoadTables.UseVisualStyleBackColor = true;
            this.cmdLoadTables.Click += new System.EventHandler(this.cmdLoadTables_Click);
            // 
            // cmdBuildPocos
            // 
            this.cmdBuildPocos.Enabled = false;
            this.cmdBuildPocos.Location = new System.Drawing.Point(179, 599);
            this.cmdBuildPocos.Name = "cmdBuildPocos";
            this.cmdBuildPocos.Size = new System.Drawing.Size(75, 23);
            this.cmdBuildPocos.TabIndex = 7;
            this.cmdBuildPocos.Text = "Build Pocos";
            this.cmdBuildPocos.UseVisualStyleBackColor = true;
            this.cmdBuildPocos.Click += new System.EventHandler(this.cmdBuildPocos_Click);
            // 
            // cmdBuildRepos
            // 
            this.cmdBuildRepos.Enabled = false;
            this.cmdBuildRepos.Location = new System.Drawing.Point(746, 599);
            this.cmdBuildRepos.Name = "cmdBuildRepos";
            this.cmdBuildRepos.Size = new System.Drawing.Size(75, 23);
            this.cmdBuildRepos.TabIndex = 8;
            this.cmdBuildRepos.Text = "Build Repos";
            this.cmdBuildRepos.UseVisualStyleBackColor = true;
            this.cmdBuildRepos.Click += new System.EventHandler(this.cmdBuildRepos_Click);
            // 
            // cmdWriteRepos
            // 
            this.cmdWriteRepos.Enabled = false;
            this.cmdWriteRepos.Location = new System.Drawing.Point(1142, 599);
            this.cmdWriteRepos.Name = "cmdWriteRepos";
            this.cmdWriteRepos.Size = new System.Drawing.Size(75, 23);
            this.cmdWriteRepos.TabIndex = 9;
            this.cmdWriteRepos.Text = "Write Repos";
            this.cmdWriteRepos.UseVisualStyleBackColor = true;
            this.cmdWriteRepos.Click += new System.EventHandler(this.cmdWriteRepos_Click);
            // 
            // cmdWritePocos
            // 
            this.cmdWritePocos.Enabled = false;
            this.cmdWritePocos.Location = new System.Drawing.Point(602, 599);
            this.cmdWritePocos.Name = "cmdWritePocos";
            this.cmdWritePocos.Size = new System.Drawing.Size(75, 23);
            this.cmdWritePocos.TabIndex = 10;
            this.cmdWritePocos.Text = "Write Pocos";
            this.cmdWritePocos.UseVisualStyleBackColor = true;
            this.cmdWritePocos.Click += new System.EventHandler(this.cmdWritePocos_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(871, 19);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(94, 13);
            this.label3.TabIndex = 11;
            this.label3.Text = "Path to Repo Files";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(488, 19);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 13);
            this.label4.TabIndex = 12;
            this.label4.Text = "Path to Poco Files";
            // 
            // txtPathToRepos
            // 
            this.txtPathToRepos.Location = new System.Drawing.Point(874, 35);
            this.txtPathToRepos.Name = "txtPathToRepos";
            this.txtPathToRepos.Size = new System.Drawing.Size(363, 20);
            this.txtPathToRepos.TabIndex = 13;
            this.txtPathToRepos.Text = "C:\\TestData\\CrudRepos";
            // 
            // txtPathToPocos
            // 
            this.txtPathToPocos.Location = new System.Drawing.Point(491, 35);
            this.txtPathToPocos.Name = "txtPathToPocos";
            this.txtPathToPocos.Size = new System.Drawing.Size(353, 20);
            this.txtPathToPocos.TabIndex = 14;
            this.txtPathToPocos.Text = "C:\\TestData\\CrudPocos";
            // 
            // txtPocoNamespace
            // 
            this.txtPocoNamespace.Location = new System.Drawing.Point(272, 19);
            this.txtPocoNamespace.Name = "txtPocoNamespace";
            this.txtPocoNamespace.Size = new System.Drawing.Size(194, 20);
            this.txtPocoNamespace.TabIndex = 16;
            this.txtPocoNamespace.Text = "Pocos";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(269, 3);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(92, 13);
            this.label5.TabIndex = 15;
            this.label5.Text = "Poco Namespace";
            // 
            // txtRepoNamespace
            // 
            this.txtRepoNamespace.Location = new System.Drawing.Point(272, 60);
            this.txtRepoNamespace.Name = "txtRepoNamespace";
            this.txtRepoNamespace.Size = new System.Drawing.Size(194, 20);
            this.txtRepoNamespace.TabIndex = 18;
            this.txtRepoNamespace.Text = "Repos";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(269, 44);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(93, 13);
            this.label6.TabIndex = 17;
            this.label6.Text = "Repo Namespace";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1273, 634);
            this.Controls.Add(this.txtRepoNamespace);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtPocoNamespace);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtPathToPocos);
            this.Controls.Add(this.txtPathToRepos);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmdWritePocos);
            this.Controls.Add(this.cmdWriteRepos);
            this.Controls.Add(this.cmdBuildRepos);
            this.Controls.Add(this.cmdBuildPocos);
            this.Controls.Add(this.cmdLoadTables);
            this.Controls.Add(this.txtRepos);
            this.Controls.Add(this.txtPocos);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lstTables);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtConString);
            this.Name = "Form1";
            this.Text = "Repo Generator";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtConString;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox lstTables;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RichTextBox txtPocos;
        private System.Windows.Forms.RichTextBox txtRepos;
        private System.Windows.Forms.Button cmdLoadTables;
        private System.Windows.Forms.Button cmdBuildPocos;
        private System.Windows.Forms.Button cmdBuildRepos;
        private System.Windows.Forms.Button cmdWriteRepos;
        private System.Windows.Forms.Button cmdWritePocos;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtPathToRepos;
        private System.Windows.Forms.TextBox txtPathToPocos;
        private System.Windows.Forms.TextBox txtPocoNamespace;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtRepoNamespace;
        private System.Windows.Forms.Label label6;
    }
}

