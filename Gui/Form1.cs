﻿using DapperRepoDalGenerator;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Gui
{
    public partial class Form1 : Form
    {
        List<TableDef> TableDefs;
        Dictionary<string, List<string>> AllPocos;
        Dictionary<string, List<string>> AllRepos;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            TableDefs = new List<TableDef>();
            AllRepos = new Dictionary<string, List<string>>();
            AllPocos = new Dictionary<string, List<string>>();
        }

        private void cmdLoadTables_Click(object sender, EventArgs e)
        {
            if (txtConString.Text.Length < 5)
            {
                MessageBox.Show("Incorrect connection string");
                return;
            }
            try
            {
                TableDefs.Clear();
                lstTables.Items.Clear();
                txtPocos.Text = "";
                txtRepos.Text = "";

                IDbAccess db = new DbAccess(txtConString.Text);
                List<string> allTables = db.GetListTables();
                TableDefinitionBuilder td = new TableDefinitionBuilder(db);
                
                foreach (string table in allTables)
                {
                    lstTables.Items.Add(table);
                    TableDef def = td.BuildTableDef(table);
                    TableDefs.Add(def);
                }

                cmdBuildPocos.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error retrieving tables from DB -- " + ex.Message);
            }
        }

        private void cmdBuildPocos_Click(object sender, EventArgs e)
        {
            if (txtPocoNamespace.Text.Length < 3)
            {
                MessageBox.Show("Poco namespace too short");
                return;
            }
            try
            {
                txtPocos.Text = "";

                IDbAccess db = new DbAccess(txtConString.Text);

                foreach (TableDef def in TableDefs)
                {                    
                    PocoBuilder pb = new PocoBuilder(txtPocoNamespace.Text);
                    List<string> lines = pb.BuildPocoDef(def);
                    AllPocos.Add(def.tableName, lines);
                    txtPocos.Text += "Table Name = " + def.tableName + Environment.NewLine;
                    foreach(string line in lines)
                    {
                        txtPocos.Text += line + Environment.NewLine;
                    }
                    txtPocos.Text += Environment.NewLine;
                }

                cmdBuildRepos.Enabled = true;
                cmdWritePocos.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error retrieving tables from DB -- " + ex.Message);
            }
        }

        private void cmdBuildRepos_Click(object sender, EventArgs e)
        {
            if (txtPocoNamespace.Text.Length < 3 || txtRepoNamespace.Text.Length < 3)
            {
                MessageBox.Show("Must have valid poco and repo namespaces");
                return;
            }
            try
            {
                txtRepos.Text = "";


                foreach (TableDef def in TableDefs)
                {
                    RepoBuilder rb = new RepoBuilder(def, txtPocoNamespace.Text, txtRepoNamespace.Text);
                    List<string> lines = rb.BuildRepoDef(def);
                    AllRepos.Add(def.tableName, lines);
                    txtRepos.Text += "Table Name = " + def.tableName + Environment.NewLine;
                    foreach (string line in lines)
                    {
                        txtRepos.Text += line + Environment.NewLine;
                    }
                    txtRepos.Text += Environment.NewLine;
                }
                cmdWriteRepos.Enabled = true;
            }
            catch (Exception ex)
            {
                MessageBox.Show("Error retrieving tables from DB -- " + ex.Message);
            }
        }

        private void cmdWritePocos_Click(object sender, EventArgs e)
        {
            if (txtPathToPocos.Text.Length < 5)
            {
                MessageBox.Show("Invalid path to write pocos");
                return;
            }

            if (!Directory.Exists(txtPathToPocos.Text))
            {
                Directory.CreateDirectory(txtPathToPocos.Text);
            }

            string basePath;
            if (txtPathToPocos.Text.Substring(txtPathToPocos.Text.Length -1, 1) != "\\")
            {
                basePath = txtPathToPocos.Text + "\\";
            }
            else
            {
                basePath = txtPathToPocos.Text;
            }

            string path;
            //if (txtPathToPocos.Text.)
            foreach (KeyValuePair<string, List<string>> def in AllPocos)
            {
                path = basePath + def.Key + ".cs";
                if(File.Exists(path))
                { 
                    File.Delete(path); 
                }
                File.WriteAllLines(path, def.Value);
            }
        }

        private void cmdWriteRepos_Click(object sender, EventArgs e)
        {
            if (txtPathToRepos.Text.Length < 5)
            {
                MessageBox.Show("Invalid path to write Repos");
                return;
            }

            if (!Directory.Exists(txtPathToRepos.Text))
            {
                Directory.CreateDirectory(txtPathToRepos.Text);
            }

            string basePath;
            if (txtPathToRepos.Text.Substring(txtPathToRepos.Text.Length - 1, 1) != "\\")
            {
                basePath = txtPathToRepos.Text + "\\";
            }
            else
            {
                basePath = txtPathToRepos.Text;
            }

            string path;
            foreach (KeyValuePair<string, List<string>> def in AllRepos)
            {
                path = basePath + def.Key + "Repo.cs";
                if (File.Exists(path))
                {
                    File.Delete(path);
                }
                File.WriteAllLines(path, def.Value);
            }
        }
    }
}
